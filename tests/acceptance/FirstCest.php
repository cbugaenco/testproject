<?php

use Helper\Acceptance;
use Page\Filters;


class FirstCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage(Filters::$URL);
    }

    // tests
    /**
     * @throws \Codeception\Exception\ModuleException+
     */

    // тест проверяет работу фильтров "Комфорт"
    public function testCheckFilters(Filters $filters, Acceptance $acceptance): void
    {
        $filters->checkActivatedFilters($acceptance);
    }
}
