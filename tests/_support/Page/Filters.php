<?php
namespace Page;


use Helper\Acceptance;

class Filters
{
    // include url of current page

    public static $URL = 'ro/list/transport/cars';
    public static $mainGrid = '//body[@class= "page page-index popmechanic-desktop"]';
    public $I;

    public static function route($param)
    {
        return static::$URL.$param;
    }

    public function __construct(\AcceptanceTester $I){
        $this->I = $I;
    }

    public function chooseComfortFilter(...$filterID): array
    {
        $names = array();
        $I = $this->I;
        foreach ($filterID as $filter){
            // получает название выбранного фильтра
            $names[] = $I->grabTextFrom('//input[@id="feature-' . $filter . '"]//following-sibling::span');
            // нажимает на выбранный фильтр
            $I->click('//input[@id="feature-'.$filter.'"]');
            // ждет формирования списка объявлений
            $I->waitForElement(self::$mainGrid);
        }
        // выводит названия выбранных фильтров на экран
        print_r($names);
        return $names;
    }

    /**
     * @throws \Codeception\Exception\ModuleException
     */
    public function checkActivatedFilters($acceptance): void
    {
        $I = $this->I;
        // нажимает на фильтр "Комфорт"
        $I->click('//label[@for="filter-1077"]');
        // ждет формирования списка фильтров "Комфорт"
        $I->waitForElementVisible('//ul[@class="items__filters__filter__content__list"]');
        // возвращает фильтры по ID, можно вписать любые из списка "Комфорт"
        $textFilters = $this->chooseComfortFilter('130', '135', '132');
        // генерирует случайное число от 0 до числа количества объявлений
        $num = mt_rand(0,$acceptance->countElements());
        echo "Random number of ad: ".$num;
        echo "\n";
        // получает атрибут "href" элемента, рандомного объявления $num
        $eld = $I->grabMultiple('//a[@class="   "]', 'href');
        // открывает страницу случайного объявления по номеру сгенерированному ранее
        $I->amOnPage($eld[$num]);
        // проверяет, что на странице объявления присутствует текст выбранных фильтров
        foreach ($textFilters as $tf){
            $I->see($tf);
        }
    }
}
