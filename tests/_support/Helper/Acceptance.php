<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Codeception\Module;

class Acceptance extends Module
{
    /**
     * @throws \Codeception\Exception\ModuleException
     */
    // возвращает количество объявлений на странице
    public function countElements()
    {
        $els = $this->getModule('WebDriver')->_findElements('//li[@class="ads-list-photo-item   "]');
        echo "Total ads on page ".count($els);
        echo "\n";
        return count($els);
    }
}
